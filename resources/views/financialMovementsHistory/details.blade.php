@extends('layouts.master')

@section('title', 'Financial Movement Details')

@section('content')
<h2>Financial Movement Details</h2>
<div class="card">
	@if(isset($financialMovement))
	<div class="card-body">
		<h5 class="card-title">{{ 'Wallet #' . $financialMovement['walletId'] . ' Financial Movement' }}</h5>
	</div>
	<ul class="list-group list-group-flush">
		<li class="list-group-item">
			<p class="font-weight-bold">ID</p> 
			{{ $financialMovement['id'] }}
		</li>
		<li class="list-group-item">
			<p class="font-weight-bold">Booking Date</p>  
			{{ $financialMovement['bookingDate'] }}
		</li>
		<li class="list-group-item">
			<p class="font-weight-bold">Ordering Account Number</p>
			{{ $financialMovement['orderingAccountNumber'] }}
		</li>
		<li class="list-group-item">
			<p class="font-weight-bold"> Beneficiary AccountNumber</p>
			{{ $financialMovement['beneficiaryAccountNumber'] }}
		</li>

		<li class="list-group-item">
			<p class="font-weight-bold">Currency</p>
			{{ $financialMovement['orderingAmount']['currency'] }}
		</li>

		<li class="list-group-item">
			<p class="font-weight-bold">Amount</p>
			{{ $financialMovement['orderingAmount']['value'] }}
		</li>

		<li class="list-group-item">
			<p class="font-weight-bold">Exchange Rate</p>
			{{ $financialMovement['exchangeRate'] }}
		</li>

		<li class="list-group-item">
			<p class="font-weight-bold">description</p>
			{{ $financialMovement['description'] }}
		</li>

	</ul>
	@else
	<div class="card-body">
		<h5 class="card-title">No Corresponding Financial Movement was found</h5>
	</div>
	@endif
	<div class="card-body">
		<a href="{{ route('financialMovementsHistory.index') }}" class="card-link">Back To Financial Movements List</a>
	</div>
</div>
@endsection