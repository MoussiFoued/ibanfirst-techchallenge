@extends('layouts.master')

@section('title', 'Financial Movements History')

@section('content')
	<h2>Wallet Financial Movements History</h2>
	<p>Request the list of financial movements that has been received or sent on a specific period of time.</p>  
	<div class="form-inline form-group">
		<input type="date" class="form-control mr-sm-2" id="from_date" name="from_date" value="{{ Request::query('from_date') }}" placeholder="From Date">

		<input type="date" class="form-control mr-sm-2" id="to_date" name="to_date" value="{{ Request::query('to_date') }}" placeholder="To Date">

		<input type="number" min="1" class="form-control mr-sm-2" id="per_page" name="per_page" value="{{ Request::query('per_page') }}" placeholder="Per Page">
		<select id="sort_by" name="sort_by" class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
			<option value="">Sort By</option>
			<option value="ASC" {{ Request::query('sort_by') === 'ASC' ? 'selected' : '' }}>ASC</option>
			<option value="DESC" {{ Request::query('sort_by') === 'DESC' ? 'selected' : '' }}>DESC</option>
		</select> 
		<a class="float-right mr-0" href="{{ route('wallets.index') }}">Back to Wallet List</a> 
	</div>        
	<table class="table table-hover">
		<thead>
			<tr>
				<th>ID</th>
				<th>Wallet ID</th>
				<th>Booking Date</th>
				<th>Currency</th>
				<th>Amount</th>
				<th>Description</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			@forelse($financialMovements as $mv)
			<tr>
				<td>{{ $mv['id'] }}</td>
				<td>{{ $mv['walletId'] }}</td>
				<td>{{ $mv['bookingDate'] }}</td>
				<td>{{ $mv['amount']['currency'] }}</td>
				<td>{{ $mv['amount']['value'] }}</td>
				<td>{{ $mv['description'] ?? 'No Data Found' }}</td>
				<td>
					<a href="{{ route('financialMovementsHistory.details', $mv['id']) }}">Show Detail</a>
				</td>
			</tr>
			@empty
			<tr>
				<td colspan="7">
					No Corresponding Financial Movements were found
				</td>
			</tr>
			@endforelse
		</tbody>
	</table>
	@if(!empty($financialMovements))
		{{ $financialMovements->links() }}
	@endif

@endsection

@section('scripts')
    <script>
  		var url = '{{ url()->current() }}';
    	
    	$('input#from_date, input#to_date, input#per_page, select#sort_by').change(function(){
    		
    		var queryParams = { 
  				from_date: $('input#from_date').val(), 
  				to_date: $('input#to_date').val(), 
  				per_page: $('input#per_page').val(), 
  				sort_by: $('select#sort_by').children("option:selected").val(),
  				page: "{{ Request::query('page') ?? '1' }}",
  			};

			var strQueryParams = $.param(queryParams);

  			window.location.href = url + '?' + strQueryParams;

		});
    	
    </script>
@endsection



