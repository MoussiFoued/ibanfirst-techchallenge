@extends('layouts.master')

@section('title', 'Wallet List')

@section('content')
	<h2>Wallet List</h2>
	<p>With the Retrieve wallet list service, you can list obtain the list of all wallet account hold with IBANFIRST. The object return in the Array is a simplified version of the Wallet providing you the main information on the wallet without any additional request.</p>  
	<div class="form-inline form-group">
		<input type="number" min="1" class="form-control mr-sm-2" id="per_page" name="per_page" value="{{ Request::query('per_page') }}" placeholder="Per Page">
		<select id="sort_by" name="sort_by" class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
			<option value="">Sort By</option>
			<option value="ASC" {{ Request::query('sort_by') === 'ASC' ? 'selected' : '' }}>ASC</option>
			<option value="DESC" {{ Request::query('sort_by') === 'DESC' ? 'selected' : '' }}>DESC</option>
		</select>
		<a class="pull-right" href="{{ route('financialMovementsHistory.index') }}"> Financial Movements History</a>  
	</div>        
	<table class="table table-hover">
		<thead>
			<tr>
				<th>ID</th>
				<th>Tag</th>
				<th>Currency</th>
				<th>Amount</th>
				<th>Date Last Financial Movement</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			@forelse($wallets as $wallet)
			<tr>
				<td>{{ $wallet['id'] }}</td>
				<td>{{ $wallet['tag'] }}</td>
				<td>{{ $wallet['currency'] }}</td>
				<td>{{ $wallet['valueAmount']['value'] }}</td>
				<td>{{ $wallet['dateLastFinancialMovement'] ?? 'No Data Found' }}</td>
				<td>
					<a href="{{ route('wallet.details', $wallet['id']) }}">Show Detail</a>
				</td>
			</tr>
			@empty
			<tr>
				<td colspan="6">
					No Corresponding Wallets were found
				</td>
			</tr>
			@endforelse
		</tbody>
	</table>
	@if(!empty($wallets))
		{{ $wallets->links() }}
	@endif

@endsection

@section('scripts')
    <script>
  		var url = '{{ url()->current() }}';
    	
    	$('input#per_page, select#sort_by').change(function(){
    		
    		var queryParams = { 
  				per_page: $('input#per_page').val(), 
  				sort_by: $('select#sort_by').children("option:selected").val(),
  				page: "{{ Request::query('page') }}",
  			};

			var strQueryParams = $.param(queryParams);

  			window.location.href = url + '?' + strQueryParams;

		});
    	
    </script>
@endsection



