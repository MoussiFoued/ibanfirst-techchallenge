@extends('layouts.master')

@section('title', 'Wallet Details')

@section('content')
<h2>Wallet Details</h2>
<div class="card">
	@if(isset($wallet))
	<div class="card-body">
		<h5 class="card-title">{{ $wallet['holder']['name'] . '\'s Wallet' }}</h5>
	</div>
	<ul class="list-group list-group-flush">
		<li class="list-group-item">
			<p class="font-weight-bold">ID</p> 
			{{ $wallet['id'] }}
		</li>
		<li class="list-group-item">
			<p class="font-weight-bold">Tag</p>  
			{{ $wallet['tag'] }}
		</li>
		<li class="list-group-item">
			<p class="font-weight-bold">Currency</p>
			{{ $wallet['currency'] }}
		</li>
		<li class="list-group-item">
			<p class="font-weight-bold">Account Number</p>
			{{ $wallet['accountNumber'] }}
		</li>

		<li class="list-group-item">
			<p class="font-weight-bold">Holder Address</p>
			{{ implode(" ", $wallet['holder']['address']) }}
		</li>

		<li class="list-group-item">
			<p class="font-weight-bold">Correspondent Bank Name</p>
			{{ $wallet['correspondentBank']['name'] }}
		</li>

		<li class="list-group-item">
			<p class="font-weight-bold">Correspondent Bank Address</p>
			{{ implode(" ", $wallet['correspondentBank']['address']) }}
		</li>

		<li class="list-group-item">
			<p class="font-weight-bold">Holder Bank Name</p>
			{{ $wallet['holderBank']['name'] }}
		</li>

		<li class="list-group-item">
			<p class="font-weight-bold">Holder Bank Address</p>
			{{ implode(" ", $wallet['holderBank']['address']) }}
		</li>
	</ul>
	@else
	<div class="card-body">
		<h5 class="card-title">No Corresponding Wallet was found</h5>
	</div>
	@endif
	<div class="card-body">
		<a href="{{ route('wallets.index') }}" class="card-link">Back To Wallet List</a>
	</div>
</div>
@endsection