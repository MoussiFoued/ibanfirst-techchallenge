<!DOCTYPE html>
<html lang="en">
<head>
	<title>IbanFirst - @yield('title')</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>
<body>
	<div class="container mt-4">
		@if(isset($error))
		<div class="alert alert-warning mt-4 mb-4" role="alert">
			<h4 class="alert-heading">Oops!</h4>
			<p>{{ $error }}</p>
		</div>
		@endif

		@yield('content')
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	@yield('scripts')
</body>
</html>
