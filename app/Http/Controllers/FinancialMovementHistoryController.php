<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;

class FinancialMovementHistoryController extends Controller
{
	/**
     * Retrieve financial movements history.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Illuminate\View\View
     */
	public function financialMovementsHistory(Request $request)
	{
		$fromDate = $request->from_date;
		$toDate = $request->to_date;

		$perPage = ($request->per_page > 0) ? $request->per_page : 10;

		$sortBy = $request->sort_by;
		
		$page = (int) $request->page;

		try {

			$response = Http::withHeaders($this->header)
			->get(self::API_ENDPOINT . "/financialMovements/", [
					'fromDate' => $fromDate,
					'toDate' => $toDate,
					'sort' => $sortBy
				]
			);

		} catch (\Exception $e) {

			return view('financialMovementsHistory.index')
			->with('error', $e->getMessage())
			->with('financialMovements', []);
		}

		$wallets = $response->json()['financialMovements'] ?? [];

		$offset = ($page * $perPage) - $perPage;

		$paginator = new LengthAwarePaginator(
			array_slice($wallets, $offset, $perPage, true),
			count($wallets),
			$perPage,
			$page,
			['path' => $request->url(), 'query' => $request->query()]
		);


		return view('financialMovementsHistory.index', [
			'financialMovements' => $paginator
		]);
	}

	/**
     * Retrieve a particular financial movement details.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Illuminate\View\View
     */
	public function financialMovementDetails(Request $request, $id)
	{
		$fromDate = $request->from_date;
		$toDate = $request->to_date;

		$perPage = ((int) $request->per_page > 0) ? $request->per_page : 10;

		$sortBy = $request->sort_by;
		
		$page = (int) $request->page;

		try {

			$response = Http::withHeaders($this->header)
			->get(self::API_ENDPOINT . "/financialMovements/-{$id}"
			);

		} catch (\Exception $e) {

			return view('financialMovementsHistory.details')
			->with('error', $e->getMessage());
		}

		return view('financialMovementsHistory.details', [
			'financialMovement' => $response->json()['financialMovement'] ?? null
		]);
	}
}
