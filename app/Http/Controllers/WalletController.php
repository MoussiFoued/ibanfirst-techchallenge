<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;

class WalletController extends Controller
{	
	 /**
     * Get wallet list.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Illuminate\View\View
     */
	 public function wallets(Request $request)
	 {
	 	$perPage = ($request->per_page > 0) ? $request->per_page : 10;

	 	$sortBy = $request->sort_by;

	 	$page = (int) $request->page;

	 	try {

	 		$response = Http::withHeaders($this->header)
	 		->get(self::API_ENDPOINT . "/wallets/",  [
	 			'sort' => $sortBy
	 		]);

	 	} catch(\Exception $e) {

	 		return view('wallets.index')
	 		->with('error', $e->getMessage())
	 		->with('wallets', []);

	 	}

	 	$wallets = $response->json()['wallets'] ?? [];

	 	$offset = ($page * $perPage) - $perPage;

	 	$paginator = new LengthAwarePaginator(
	 		array_slice($wallets, $offset, $perPage, true),
	 		count($wallets),
	 		$perPage,
	 		$page,
	 		['path' => $request->url(), 'query' => $request->query()]
	 	);

	 	return view('wallets.index', [
	 		'wallets' => $paginator,
	 	]);
	 }

	/**
     * Get wallet list.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Illuminate\View\View
    */
	public function wallet(Request $request, $id)
	{
		try {

			$response = Http::withHeaders($this->header)
			->get(self::API_ENDPOINT . "/wallets/-{$id}");

		} catch (\Exception $e) {

			return view('wallets.detail')
			->with('error', $e->getMessage());
		}

		return view('wallets.details', [
			'wallet' => $response['wallet'] ?? null
		]);
	}

}
