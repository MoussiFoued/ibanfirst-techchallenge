<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    const API_ENDPOINT = 'https://sandbox2.ibanfirst.com/api';

	protected $header;

	public function __construct() {
		$this->header = generateXWSSEHeader();
	}

	public function getHeader()
	{
		return $this->header;
	}
}
