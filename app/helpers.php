<?php

function generateXWSSEHeader() {
    // Login informations
	$username = "a00720d" ;			// The username used to authenticate
	$password = "6KPPczga4H6pR+ZeMj+iQ5UpB0foUoO3hQWOjUiYkESU3HGLfXwce8He7TfwY/k4c3hAcFViIFfKUC+GwcbYsQ==" ;				// The password used to authenticate
	$nonce = "" ;					// The nonce
	$nonce64 = "" ;					// The nonce with a Base64 encoding
	$date = "" ;					// The date of the request, in  ISO 8601 format
	$digest = "" ;					// The password digest needed to authenticate you
	$header = "" ; 					// The final header to put in your request

	// Making the nonce and the encoded nonce
	$chars = "0123456789abcdef";
	for ($i = 0; $i < 32; $i++) {
		$nonce .= $chars[rand(0, 15)];
	}
	$nonce64 = base64_encode($nonce) ;

	// Getting the date at the right format (e.g. YYYY-MM-DDTHH:MM:SSZ)
	$date = gmdate('c');
	$date = substr($date,0,19)."Z" ;

	// Getting the password digest
	$digest = base64_encode(sha1($nonce.$date.$password, true));

	// Getting the X-WSSE header to put in your request
	$header = [];
	$header['X-WSSE'] = sprintf('UsernameToken Username="%s", PasswordDigest="%s", Nonce="%s", Created="%s"',$username, $digest, $nonce64, $date);

	return $header;
}