<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Retreive list of wallets
Route::get('/', 'WalletController@wallets')->name('wallets.index');

//Display one single wallet details
Route::get('/wallets/{id}', 'WalletController@wallet')->name('wallet.details');

// Retrieve financial movements history
Route::get('/financialMovements', 'FinancialMovementHistoryController@financialMovementsHistory')->name('financialMovementsHistory.index');

// Request information on a particular financial movement that has been credited or debited to a wallet.
Route::get('/financialMovements/{id}', 'FinancialMovementHistoryController@financialMovementDetails')->name('financialMovementsHistory.details');



// Redirect to Rick Astley - Never Gonna Give You Up - ♩ ♪ ♫ ♬ ♭
Route::redirect('.env', 'https://www.youtube.com/watch?v=dQw4w9WgXcQ');
