# iBanFirst - Tech Challenge


### Laravel 7.x example codebase containing real world implementation for IbanFirst Wallets & Financial movements APIs.


----------

# Getting started

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/7.x/installation)


Clone the repository

    git clone https://MoussiFoued@bitbucket.org/MoussiFoued/ibanfirst-techchallenge.git

Switch to the repo folder

    cd ibanfirst-techchallenge

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

**TL;DR command list**

    git clone https://MoussiFoued@bitbucket.org/MoussiFoued/ibanfirst-techchallenge.git
    cd ibanfirst-techchallenge
    composer install
    cp .env.example .env
    php artisan key:generate
    php artisan serve
    
## Dependencies

- [guzzlehttp/guzzle](https://github.com/guzzle/guzzle) - PHP HTTP client library

----------

# Testing

To run the application tests, you can run the following from your project root:

    ./vendor/bin/phpunit

