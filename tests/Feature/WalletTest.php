<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Http\Controllers\WalletController;
use Illuminate\Support\Facades\Http;

class WalletTest extends TestCase
{
    protected $header;
    protected $endPoint;

    protected function setUp():void
    {
        parent::setUp();
        $walletCtrl = new WalletController();
        $this->header = $walletCtrl->getHeader();
        $this->endPoint = $walletCtrl::API_ENDPOINT;
    }

    /** @test */
    public function canFetchWalletsList()
    {
        $response = $this->get('/');
        $response
        ->assertViewIs('wallets.index')
        ->assertStatus(200);
    }

    /** @test */
    public function canSeeWalletDetail()
    {
        $apiResponse = Http::withHeaders($this->header)
        ->get($this->endPoint . "/wallets/");

        $wallets = $apiResponse->json()['wallets'];

        $firstWalletID = $wallets[0]['id'];

        $response = $this->get('/wallets/{$firstWalletID}');
        $response
        ->assertViewIs('wallets.details')
        ->assertStatus(200);
    }
}